// modules
const fs  =require ("fs")
const csv = require('csvtojson')

const matchesCsfFile = '../data/matches.csv'
const deliveriesCsvFile= '../data/deliveries.csv'

function csvToJson(csvFile){
    csv()
    .fromFile(csvFile)
    .then((json)=>{
        let outPutFilePath = csvFile.replace('.csv','.json')
        console.log(outPutFilePath)
        fs.writeFileSync(outPutFilePath,JSON.stringify(json))
       
    })
    
}

csvToJson(matchesCsfFile)
csvToJson(deliveriesCsvFile)
