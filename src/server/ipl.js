import fs from 'fs'

// function for creating file

function createFile(data,filename){
    let path = `../public/output/${filename}`
    const jsonString = JSON.stringify(data)
    fs.writeFileSync(path, jsonString)
}




// problem functions

export function matchesPlayesForSeason(matches){

    let MatchCountArray = []
    let matchCount = {}

    matches.forEach((match)=>{
        matchCount[match["season"]] = matchCount[match["season"]] +1 ||1
    })

    MatchCountArray.push(matchCount)
    createFile(MatchCountArray,'matchesPerYear.json')
}








